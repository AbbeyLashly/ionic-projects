( function() {
	// Recupera o library de controller
	angular.module( "pechincha.controller" )
	
	// Instancia do controller
	.controller( 'PropostaController', 
		[ '$scope', '$pickup', '$cordovaGeolocation', '$ionicModal', '$ionicPopup', 'CacheService', 'PropostaModel', 'ProdutoModel', 
	function( $scope, $pickup, $cordovaGeolocation, $ionicModal, $ionicPopup, CacheService, PropostaModel, ProdutoModel )
	{
		// Referência para o Controller ser reconhecido por métodos fora Contexto
		var self = this ;
		
		// Buffer da lista das Propostas
		self.Propostas = [] ;
		
		// Buffer do registro de uma Proposta
		self.Proposta = {} ;
		
		// Buffer do registro de um Pechincha
		self.Pechincha = {} ;
		
		// Registro com a melhor oferta
		self.BestOffer = null ;
		
		// Buffer do registro de um Fornecedor
		self.Fornecedor = null ;
		
		// Métodos para pickup
		self.modal = {
			// Modal para inclusão do Registro
			new: function() {
				$ionicModal.fromTemplateUrl( 'view/app/proposta-form.html', { 
					animation: 'slide-in-up',
					scope: $scope,
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Reseta o formulário
					self.Proposta = {} ;
					self.Fornecedor = null ;
					
					// Exibe o modal
					self.newPropostaModal = modal ;
					self.newPropostaModal.show() ;
				});
			},
			// Modal para visão do Registro
			view: function( object ) {
				CacheService.set( "PropostaView", object ) ;
				$pickup.pickProposta( function(){
					self.init.run() ;
				}) ;
			},
			// Modal para edição do Registro
			update: function( object ) {
				$ionicModal.fromTemplateUrl( 'view/app/proposta-form.html', { 
					animation: 'slide-in-up',
					scope: $scope,
					backdropClickToClose: false
				})
				.then( function( modal ) {
					// Reseta o formulário
					angular.copy( object, self.Proposta ) ;

					// Configura o objeto do Fornecedor
					self.Fornecedor = {
						CodFornecedor: self.Proposta.CodFornecedor,
						Nome: self.Proposta.NomeFornecedor,
						Endereco: self.Proposta.EnderecoFornecedor
					} ;
					
					// Exibe o modal
					self.newPropostaModal = modal ;
					self.newPropostaModal.show() ;
				});
			},
			// Modal para visão do Registro
			remove: function( object ) {
				if( ! object.Locked ) {
					$ionicPopup.confirm({
						title: 'Atenção',
     					template: 'Confirma a remoção do Registro ?'
					}).then( function( confirm ){
						if( confirm ) {
							PropostaModel.remove( object, function( isremoved ) {
								if( isremoved ) {
									self.init.run() ;
								}
								else {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível remover o Registro. Tente Novamente !"
									});
								}
							}) ;
						}
					}) ;
				}
				else
				{
					$ionicPopup.alert({
					    title: 'Atenção',
					    subTitle: 'O objeto não pode ser removido pois está sendo referenciado por outros registros'
					});
				}
			},
			// Fecha os detalhes da Rota e a inicia, caso marcada
			persist: function( ppForm ) {
				// Caso exista formulário para persistir os dados
				if( ppForm ) {
					// Container para os campos com erro
					var eFields = [] ;
					
					if( ! self.Fornecedor ) eFields.push( "Fornecedor" ) ;
					
					// Verifica se o formulário é válido
					if( ! eFields.length ) {
						// Caso seja para fazer update
						if( self.Proposta.CodProposta )
						{
							// Remove as informações desnecessárias para o form
							delete self.Proposta.Locked ;
							delete self.Proposta.Produtos ;
							delete self.Proposta.NomeFornecedor ;
							delete self.Proposta.EnderecoFornecedor ;
							delete self.Proposta.TelefoneFornecedor ;
							delete self.Proposta.EmailFornecedor ;
							
							// Configura o formuárlio
							self.Proposta.CodFornecedor = self.Fornecedor.CodFornecedor ;
							
							// Realiza o update
							PropostaModel.update( self.Proposta, 
								function( persisted ){
									self.newPropostaModal.hide().then( function( modal ) {
										self.newPropostaModal.remove() ;
										self.init.run() ;
									}) ;
								}
							) ;
						}
						// Caso seja para fazer inclusão
						else {
							// Configura o formuárlio
							self.Proposta.CodPechincha = self.Pechincha.CodPechincha ;
							self.Proposta.CodFornecedor = self.Fornecedor.CodFornecedor ;

							// Realiza o insert
							PropostaModel.insert( self.Proposta, 
								function( persisted ){
									// Fecha o modal e retorna como selecionado o ítem recém persistido
									self.newPropostaModal.hide().then( function( modal ) {
										self.newPropostaModal.remove() ;
										self.init.run() ;
									}) ;
								},
								function() {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível incluir o Registro. Tente Novamente !"
									});
								}
							) ;
						}
					}
					else {
						$ionicPopup.alert({
						    title: 'Formulário com Erro',
						    subTitle: 'Por favor, verifique os campos: ' + eFields.join( ', ' )
						});
					}
				}
				// Caso o formulário seja falso, descarta o form ou objeto em edição
				else {
					// Fecha o modal e cancela a operação
					self.newPropostaModal.hide().then( function( modal ) {
						self.newPropostaModal.remove() ;
					}) ;
				}
			},
			// Método para fechar a janela de listagem
			close: function() {
				$pickup.modal( 'pickUpProposta' ).hide().then( function( modal ){
					$pickup.callback( 'calbackProposta', null ) ;
					$pickup.remove( 'calbackProposta' ) ;
				}) ;
			}
		}
		
		// Configura um picker para Fornecedor
		self.pickFornecedor = function() {
			// Setup the Pickup
			$pickup.pickFornecedor( function( fornecedor ) {
				if( fornecedor )
					self.Fornecedor = fornecedor ;
			}) ;
		}
		
		self.Produto = {
			// Configura um picker para Inclusão do Produto
			insert: function( object ) {
				// Prepara o objeto para o Formulário
				CacheService.set( 'ProdutoObject', {
					CodPechincha: object.CodPechincha,
					CodProposta: object.CodProposta
				}) ;
				// Setup the Pickup
				$pickup.formProduto( function( produto ) {
					// Atualiza a lista dos produtos de acordo com a proposta que foi alterada
					self.Produto.listByProposta( object ) ;

					// Recupera a melhor oferta
					ProdutoModel.bestOffer( self.Pechincha, function( offer ){
						if( offer ) self.BestOffer = offer ;
						else self.BestOffer = null ;
					}) ;
				}) ;
			},
			// Configura um picker para Edição do Produto
			show: function( object, proposta ) {
				// Prepara o objeto para o Formulário
				var produto = angular.copy( object, produto ) ;
				CacheService.set( 'ProdutoObject', produto ) ;

				// Setup the Pickup
				$pickup.formProduto( function( produto ) {
					// Atualiza a lista dos produtos de acordo com a proposta que foi alterada
					self.Produto.listByProposta( proposta ) ;
					
					// Recupera a melhor oferta
					ProdutoModel.bestOffer( self.Pechincha, function( offer ){
						if( offer ) self.BestOffer = offer ;
						else self.BestOffer = null ;
					}) ;
				}) ;
			},
			// Exibe uma tela específica da melhor oferta
			bestOffer: function() {
				// Abre o Modal
				$ionicModal.fromTemplateUrl( 'view/app/bestoffer.html', { 
					animation: 'slide-in-up',
					backdropClickToClose: false,
					scope: $scope
				})
				.then( function( modal ) {
					// Exibe o modal
					self.BestProdutoObjectModal = modal ;
					self.BestProdutoObjectModal.show() ;
				});
			},
			// Fecha a janela de best offer
			closeBestOffer: function() {
				self.BestProdutoObjectModal.hide().then( function(){
					self.BestProdutoObjectModal.remove() ;
					delete self.BestProdutoObjectModal ;
				})
			},
			// Métodos
			listByProposta: function( proposta ) {
				// Recupera a lista de Propostas do banco de dados
				ProdutoModel.listAllByProposta( proposta ).then( function( ret ){
					proposta.Produtos = ret.objects ;
				}) ;
			},
			stars: function( number ) {
				return new Array( number ) ;
			}
		}
		
		// Inicializa o Controller
		self.init = {
			initialized: false, 
			run: function() {
				// Recupera a Pechincha em questão
				self.Pechincha = CacheService.get( 'PropostaView' ) ;
				
				// Recupera a melhor oferta
				ProdutoModel.bestOffer( self.Pechincha, function( offer ){
					if( offer ) self.BestOffer = offer ;
					else self.BestOffer = null ;
				}) ;

				// Recupera a lista de Propostas do banco de dados
				PropostaModel.listAllByPechincha( self.Pechincha ).then( function( ret ){
					self.Propostas = ret.objects ;
					// Adiciona a lista dos produtos para a proposta em questão
					for( pidx in self.Propostas ) {
						self.Produto.listByProposta( self.Propostas[pidx] ) ;
					}

					// Marca o App como inicializado
					self.init.initialized = true ;
				}) ;
			}
		}
		
		// Atualiza a view caso não tenha sido executado o evento beforeEnter
		if( ! self.init.initialized ) { self.init.run() ; }
	}])
})() ;