( function(){
	// Recupera o library de controller
	angular.module( "pechincha.controller" )

	// Instancia do controller
	.controller( 'NavigationController', 
		[ '$pickup',
	function( $pickup )
	{
		// Variável para referencia deste objeto
		var self = this ;

		// Configura um picker para Fornecedor
		self.pickFornecedor = function() {
			// Setup the Pickup
			$pickup.pickFornecedor() ;
		}

	}]) ;
})() ;