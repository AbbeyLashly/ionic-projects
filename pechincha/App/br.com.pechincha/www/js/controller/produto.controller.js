( function() {
	// Recupera o library de controller
	angular.module( "pechincha.controller" )
	
	// Instancia do controller
	.controller( 'ProdutoController', 
		[ '$scope', '$pickup', '$ionicModal', '$ionicPopup', '$ionicLoading', '$appServices', 'CacheService', 'ProdutoModel', 
	function( $scope, $pickup, $ionicModal, $ionicPopup, $ionicLoading, $appServices, CacheService, ProdutoModel )
	{
		// Referência para o Controller ser reconhecido por métodos fora Contexto
		var self = this ;
		
		// Buffer do registro de uma Proposta
		self.Produto = {} ;
		
		// Métodos para pickup
		self.modal = {
			// Modal para visão do Registro
			remove: function() {
				$ionicPopup.confirm({
					title: 'Atenção',
 					template: 'Confirma a remoção do Registro ?'
				}).then( function( confirm ){
					if( confirm ) {
						ProdutoModel.remove( self.Produto, function( isremoved ) {
							if( isremoved ) {
								// Fecha o modal e retorna como selecionado o ítem recém persistido
								$pickup.modal( "pickUpProduto" ).hide().then( function( modal ) {
									$pickup.remove( "pickUpProduto" ) ;
									$pickup.callback( "calbackProduto" ) ;
								}) ;
							}
							else {
								$ionicPopup.alert({
								    title: 'Erro !',
								    subTitle: "Não foi possível remover o Registro. Tente Novamente !"
								});
							}
						}) ;
					}
				}) ;
			},
			// Fecha os detalhes da Rota e a inicia, caso marcada
			persist: function( prForm ) {
				// Caso exista formulário para persistir os dados
				if( prForm ) {
					// Container para os campos com erro
					var eFields = [] ;
					
					if( ! prForm.Nome.$valid ) eFields.push( "Produto" ) ;
					if( ! prForm.Preco.$valid ) eFields.push( "Preço" ) ;
					
					// Verifica se o formulário é válido
					if( ! eFields.length ) {
						// Caso seja para fazer update
						if( self.Produto.CodProduto ) {
							// Configura o formulário
							delete self.Produto.$$hashKey ;

							// Realiza o update
							ProdutoModel.update( self.Produto, 
								function( persisted ){
									// Fecha o modal e retorna como selecionado o ítem recém persistido
									$pickup.modal( "pickUpProduto" ).hide().then( function( modal ) {
										$pickup.remove( "pickUpProduto" ) ;
										$pickup.callback( "calbackProduto" ) ;
									}) ;
								}
							) ;
						}
						// Caso seja para fazer inclusão
						else {
							// Configura o formuárlio
							self.Produto.Pdc = self.Produto.Pdc ? self.Produto.Pdc : 3 ;
							self.Produto.Quantidade = self.Produto.Quantidade ? self.Produto.Quantidade : 1 ;
							self.Produto.Desconto = self.Produto.Desconto ? self.Produto.Desconto : 0 ;
							self.Produto.Parcelas = self.Produto.Parcelas ? self.Produto.Parcelas : 1 ;

							// Realiza o insert
							ProdutoModel.insert( self.Produto, 
								function( persisted ){
									// Fecha o modal e retorna como selecionado o ítem recém persistido
									$pickup.modal( "pickUpProduto" ).hide().then( function( modal ) {
										$pickup.remove( "pickUpProduto" ) ;
										$pickup.callback( "calbackProduto" ) ;
									}) ;
								},
								function() {
									$ionicPopup.alert({
									    title: 'Erro !',
									    subTitle: "Não foi possível incluir o Registro. Tente Novamente !"
									});
								}
							) ;
						}
					}
					else {
						$ionicPopup.alert({
						    title: 'Formulário com Erro',
						    subTitle: 'Por favor, verifique os campos: ' + eFields.join( ', ' )
						});
					}
				}
			},
			// Método para fechar a janela de listagem
			close: function() {
				// Fecha o modal e retorna como selecionado o ítem recém persistido
				$pickup.modal( "pickUpProduto" ).hide().then( function( modal ) {
					$pickup.remove( "pickUpProduto" ) ;
					$pickup.callback( "calbackProduto" ) ;
				}) ;
			},
			// Faz a captura da Imagem
			capture: function() {
				// Inicializa a tela de loading
				$ionicLoading.show({ template: "Aguarde ..." })
				
				setTimeout( function() {
					$appServices.pictureCapture(
						function( imageURI ) {
							$ionicLoading.hide() ;
							self.Produto.Foto = imageURI ;
						}, 
						function( err ) {
							$ionicLoading.hide() ;
							// No caso do dispositivo não possuir câmera
							if( err == "NO-CAMERA" ) {
								$ionicPopup.alert({ 
									template: "<center>O seu dispositivo não oferece este recurso</center>"
								}) ;
							}
						}
					) ;
				}, 100 ) ;
			}
		}
		
		// Inicializa o Controller
		self.init = {
			initialized: false, 
			run: function() {
				// Marca o App como inicializado
				self.init.initialized = true ;
				
				// Recupera a Produto em questão
				self.Produto = CacheService.get( 'ProdutoObject' ) ;
				self.Produto.Foto = self.Produto.Foto ? self.Produto.Foto : "img/no-image-captured.jpg" ;
			}
		}

		// Atualiza a view caso não tenha sido executado o evento beforeEnter
		if( ! self.init.initialized ) { self.init.run() ; }
	}])
})() ;