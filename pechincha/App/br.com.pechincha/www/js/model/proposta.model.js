( function(){
	angular.module( 'pechincha.model' )
	
	.factory( 'PropostaModel', 
		[ '$database', '$localstorage', '$filter', 'DBQuery',
	function( $database, $localstorage, $filter, DBQuery )
	{
		// TODO :: Criar uma execução do Promisse direto na camada do $database
		return {
			listAllByPechincha: function( pechincha ) {
				var promisse = $database.query({
					sql: DBQuery.prepareSelect({
						table: "Proposta",
						select: [ 
							'Proposta.CodPechincha as CodPechincha',
							'Proposta.CodFornecedor as CodFornecedor',
							'Proposta.CodProposta as CodProposta',
							'Proposta.Data as Data',
							'Fornecedor.Nome as NomeFornecedor',
							'Fornecedor.Endereco as EnderecoFornecedor',
							'Fornecedor.Telefone as TelefoneFornecedor',
							'Fornecedor.Email as EmailFornecedor',
							// Verifica as dependências
							"(" +
								DBQuery.prepareSelect({
									table: "Produto",
									select: [ 'COUNT(*)'],
									where: [ 
										"Produto.CodPechincha = Proposta.CodPechincha",
										"AND", 
										"Produto.CodProposta = Proposta.CodProposta"
									]
								})
							+ " ) AS Locked"
						],
						join: [{
							mode: "LEFT JOIN",
							entity: "Fornecedor",
							conditions: [ "Fornecedor.CodFornecedor = Proposta.CodFornecedor" ]
						}],
						where: [
							"Proposta.CodPechincha = ?"
						],
						order: [ "Fornecedor.Nome" ]
					}),
					data: [ pechincha.CodPechincha ],
				}) ;
				
				return promisse ;
			},
			// TODO :: Tratar todos os erros
			insert: function( form, callback ) {
				// Faz o update da Chave
				$database.query( DBQuery.pkSelfSql( "Proposta", 'CodProposta', 6, [ "CodPechincha = ?" ], [ form.CodPechincha ] ) )
				.then( function( PK ) {
					// Atribui o valor da Chave ao Form
					form.CodProposta = PK.objects[0].PK ;
					form.Data = ( $filter( 'date' )( new Date(), "yyyy-MM-dd" ) ) ;
					// Faz o insert
					$database.query( DBQuery.insertSql( "Proposta", form ) )
					.then( function(){
						// Prepara o objeto de Retorno
						$database.query({ sql: DBQuery.prepareSelect({ table: "Proposta", where: [ "Proposta.CodPechincha = ?", "AND", "Proposta.CodProposta = ?" ] } ), data: [ form.CodPechincha, form.CodProposta ] })
						.then( function( inserted ){
							// Executa o Callback com o objeto  Inserido via Parâmetro
							if( typeof( callback ) == "function" )
					   			callback( inserted.objects[0] ) ;
						}, function(){
							console.log( "Fetch Object Error" ) ;
						}) ;
					}, function(){
						console.log( "Insert Error" ) ;
					}) ;
				}, function(){
					console.log( "Fetch PK Error" ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			remove: function( Proposta, callback ) {
				$database.query( DBQuery.deleteSql({
					table: "Proposta",
					where: [ "CodPechincha = ?", "AND", "CodProposta = ?" ],
					data: [ Proposta.CodPechincha,  Proposta.CodProposta ]
				}))
				.then( function() {
					if( typeof( callback ) == "function" ) callback( true ) ;
				}, function() {
					if( typeof( callback ) == "function" ) callback( false ) ;
				}) ;
			},
			// TODO :: Tratar todos os erros
			update: function( form, callback ) {
				$database.query( DBQuery.updateSql( "Proposta", form, [ "Proposta.CodPechincha = ?", "AND", "Proposta.CodProposta = ?" ], [ form.CodPechincha, form.CodProposta ] ) )
				.then( function() {
					// Prepara o objeto de Retorno
					$database.query({ sql: DBQuery.prepareSelect({ table: "Proposta", where: [ "Proposta.CodPechincha = ?", "AND", "Proposta.CodProposta = ?" ] }), data: [ form.CodPechincha, form.CodProposta ] })
					.then( function( updated ) {
						// Executa o Callback com o objeto  Inserido via Parâmetro
						if( typeof( callback ) == "function" )
				   			callback( updated.objects[0] ) ;
					}, function() {
						console.log( "Fetch Object Error" ) ;
					}) ;
				}, function() {
					console.log( "Update Error" ) ;
				}) ;
			}
		}
	}])
})() ;