( function(){
	angular.module( "pechincha.model" )
	.factory( 'DBQuery', 
		[
	function()
	{
		// Buffer
		var buffer = {} ;
		
		// For Version 1
		var tables = [{
			name: "Pechincha",
			fields: [
				"CodPechincha CHAR(6) NOT NULL",
				"Data DATE NOT NULL",
				"Concluido INT(1) DEFAULT 0",
				"Alias VARCHAR(255) NOT NULL",
				"PRIMARY KEY( CodPechincha )"
			]
		},{
			name: "Fornecedor",
			fields: [
				"CodFornecedor CHAR(6) NOT NULL",
				"Data DATE NOT NULL",
				"Nome VARCHAR(250) NOT NULL",
				"Geoloc VARCHAR(150) NULL",
				"Endereco VARCHAR(250) NULL",
				"Telefone VARCHAR(14) NULL",
				"Email VARCHAR(250) NULL",
				"PRIMARY KEY( CodFornecedor )"
			]
		},{
			name: "Proposta",
			fields: [
				"CodPechincha CHAR(6) NOT NULL",
				"CodProposta CHAR(6) NOT NULL",
				"CodFornecedor CHAR(6) NOT NULL",
				"Data DATE NOT NULL",
				"PRIMARY KEY( CodPechincha, CodProposta )"
			]
		},{
			name: "Produto",
			fields: [
				"CodPechincha CHAR(6) NOT NULL",
				"CodProposta CHAR(6) NOT NULL",
				"CodProduto CHAR(6) NOT NULL",
				"Data DATE NOT NULL",
				"Nome VARCHAR(250) NOT NULL",
				"Preco FLOAT(10,2) NOT NULL DEFAULT 0.00",
				"Quantidade FLOAT(10,3) NOT NULL DEFAULT 0.000",
				"Desconto INT(3) NOT NULL DEFAULT 0",
				"Parcelas INT(3) NOT NULL DEFAULT 1",
				"FormaPagamento VARCHAR(50) NOT NULL DEFAULT 'DINHEIRO'",
				"Pdc INT(1) NOT NULL DEFAULT 0",
				"Anotacoes TEXT NULL",
				"Foto TEXT NULL DEFAULT NULL",
				"PRIMARY KEY( CodPechincha, CodProposta, CodProduto )"
			]
		}] ;
		
		return {
			createSql: function()
			{
				buffer.create = [] ;
				
				angular.forEach( tables, function( table ) {
					buffer.create.push({
						sql: "CREATE TABLE " + table.name + " ( " + table.fields.join( ',' ) + " )" ,
						data: []
					}) ;
				}) ;

				return buffer.create ;
			},
			dropSql: function()
			{
				buffer.drop = [] ;
				
				angular.forEach( tables, function( table ) {
					buffer.drop.push({
						sql: "DROP TABLE IF EXISTS " + table.name,
						data: []
					}) ;
				}) ;
				
				return buffer.drop ;
			},
			truncateSql: function( table )
			{
				buffer.truncate = {
					sql: "DELETE FROM " + table,
					data: []
				} ;
				
				return buffer.truncate ;
			},
			deleteSql: function( query )
			{
				// buffer da query
				var bquery = { where: [], data: [] } ;
				
				// Caso exista a função where
				if( query.where )
					bquery.where.push( query.where.join( ' ' ) )  ;
				
				// Caso exista a opção dados
				if( query.data )
					bquery.data = query.data ;
				
				buffer.remove = {
					sql: "DELETE FROM " + query.table + ( bquery.where.length ? " WHERE " : " " ) + bquery.where,
					data: bquery.data
				} ;
				
				return buffer.remove ;
			},
			pkSql: function( table, pkIncrement, pkSize, filter, filterData )
			{
				buffer.lastPkSql = {} ;
				
				buffer.lastPkSql.sql = "UPDATE " + table + " SET " + pkIncrement + " = ( SELECT CASE WHEN ( " + pkIncrement + " NOTNULL OR " + pkIncrement + " != '' ) THEN SUBSTR( '0000000000'||( " + pkIncrement + " + 1 ), -" + pkSize + ", " + pkSize + " ) ELSE SUBSTR( '0000000000'||1, -" + pkSize + ", " + pkSize + " ) END AS PK FROM " + table + " WHERE " + filter.join( " " ) + " ) WHERE " + filter.join( " " ) ;
				buffer.lastPkSql.data = filterData.concat( filterData ) ;
				
				return buffer.lastPkSql ;
			},
			pkSelfSql: function( table, pkIncrement, pkSize, filter, filterData )
			{
				buffer.lastPkSql = {} ;
				
				buffer.lastPkSql.sql = "SELECT CASE WHEN ( MAX( " + pkIncrement + " ) > 0 OR MAX( " + pkIncrement + " != '' ) ) THEN SUBSTR( '0000000000'||( MAX( " + pkIncrement + " ) + 1 ), -" + pkSize + ", " + pkSize + " ) ELSE SUBSTR( '0000000000'||1, -" + pkSize + ", " + pkSize + " ) END AS PK FROM " + table + " WHERE " + filter.join( " " ) ;
				buffer.lastPkSql.data = filterData ;
				
				return buffer.lastPkSql ;
			},
			insertSql: function( table, fields )
			{
				buffer.lastInsertSql = {} ;
				
				var keys = [], data = [], pbind = [] ;
				
				angular.forEach( fields, function( value, key ) {
					keys.push( key ) ; data.push( value ) ; pbind.push( '?' ) ;
				}) ;
				
				buffer.lastInsertSql.sql = "INSERT INTO " + table + " ( " + keys.join( ',' ) + " ) VALUES ( " + pbind.join( ',' ) + " )" ;
				buffer.lastInsertSql.data = data ;
				
				return buffer.lastInsertSql ;
			},
			updateSql: function( table, fields, filter, filterData )
			{
				buffer.lastUpdateSql = {} ;
				
				var keys = [], data = [] ;
				
				angular.forEach( fields, function( value, key ) {
					keys.push( key + "=" + '?' ) ; data.push( value ) ;
				}) ;
				
				buffer.lastUpdateSql.sql = "UPDATE " + table + " SET " + keys.join( ',' )  + " WHERE " + filter.join( " " ) ;
				buffer.lastUpdateSql.data = data.concat( filterData ) ;
				
				return buffer.lastUpdateSql ;
			},
			insertSqlCollection: function( table, collection )
			{
				// Limpa o buffer
				buffer.lastInsertSqlCollection = [] ;
				
				angular.forEach( collection, function( fields ) {
					// Gera a Query
					var keys = [], data = [], pbind = [] ;
					angular.forEach( fields, function( value, key ) {
						keys.push( key ) ; data.push( value ) ; pbind.push( '?' ) ;
					}) ;
					
					// Inclui o objeto no buffer
					buffer.lastInsertSqlCollection.push( {
						sql: "INSERT INTO " + table + " ( " + keys.join( ',' ) + " ) VALUES ( " + pbind.join( ',' ) + " )",
						data: data
					}) ;
				}) ;
				
				return buffer.lastInsertSqlCollection ;
			},
			prepareSelect: function( query )
			{
				// buffer da query
				var bquery = {
					select: [],
					join: [],
					where: [],
					group: [],
					order: [],
				} ;
				
				// Caso exista a seleção de campos
				if( query.select ) bquery.select.push( query.select.join( ',' ) ) ;
				else bquery.select = ['*'] ;
				
				// Caso exista a função join
				if( query.join ) {
					angular.forEach( query.join, function( join ) {
						bquery.join.push( join.mode + " " + join.entity + " ON " + join.conditions.join( " " ) )
					})
				}
				
				// Caso exista a função where
				if( query.where )
					bquery.where.push( query.where.join( ' ' ) )  ;
				
				// Caso exista a função group
				if( query.group )
					bquery.group.push( query.group.join( ',' ) )  ;
				
				// Caso exista a função group
				if( query.order )
					bquery.order.push( query.order.join( ',' ) )  ;
				
				bquery.sql = 
					"SELECT " + bquery.select.join( ',' ) + 
					" FROM " + query.table + 
					" " + bquery.join.join( ' ' ) + 
					( bquery.where.length ? " WHERE " : " " ) + bquery.where + 
					( bquery.group.length ? " GROUP BY " : " " ) + bquery.group +
					( bquery.order.length ? " ORDER BY " : " " ) + bquery.order
				;

				// console.log( bquery.sql ) ;
				return bquery.sql ;
			}
		} ;
	}])
})() ;