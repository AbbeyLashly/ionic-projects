( function() {
	angular.module( "pechincha" )

	.factory( 'CacheService', 
		[
	function() {
		var buffer = [] ;

		return {
			set: function( key, value ) {
				buffer[key] = value ;
			},
			get: function( key, vdefault ) {
				if( typeof( buffer[key] ) != 'undefined' )
					return buffer[key] ;
				else
					return vdefault ;
			},
			flush: function( key ) {
				buffer[key] = null ;
			}
		} ;
	}])
})() ;