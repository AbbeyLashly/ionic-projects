( function(){
	angular.module( 'agenda.controller' )

	.controller( 'ContatoFormularioController', 
		[ "ContatosModel", "Cache", "$state",
		function( ContatosModel, Cache, $state )
		{
			this.contato = angular.copy( Cache.get( "Contato" ) ) ;

			this.imagens = [
				{nome:"Batman", src:"img/batman.jpg"},
				{nome:"Stormtrooper", src:"img/stormtrooper.jpg"},
				{nome:"Harlequina", src:"img/harlequina.jpg"},
				{nome:"Yoda", src:"img/yoda.jpg"},
				{nome:"Tartarugas Ninjas", src:"img/turtle.jpg"},
			] ;

			this.guardar = function() {
				ContatosModel.guardar( this.contato ) ;
				$state.go( "app.contatos" ) ;
			}
		}
	]) ;
})() ;