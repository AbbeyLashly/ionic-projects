( function(){
	angular.module( 'agenda.model' )

	.factory( "ContatosModel", 
		[
		function(){
			var contatos = [{
				id: 1,
				nome: "Luann Francisco",
				email: "luann@spinpo.com",
				telefone: "(22) 2222 2222",
				imagem: "img/yoda.jpg"
			},{
				id: 2,
				nome: "Maria João",
				email: "maria@joao.com",
				telefone: "(22) 2222 2222",
				imagem: "img/harlequina.jpg"
			}] ;

			var metodos = {
				listarContatos: function() {
					return contatos ;
				},
				guardar: function( form ) {
					if( form.id ) {
						var idx = metodos.findIdx( form.id ) ;
						contatos[idx] = form ;
					}
					else
					{
						if( contatos.length )
							form.id = contatos[contatos.length-1].id + 1 ;
						else
							form.id = 1 ;
						
						contatos.push( form ) ;
					}
				},
				findIdx: function( id ) {
					for( idx in contatos ) {
						if( contatos[idx].id == id ) {
							return idx ;
						}
					}

					return null ;
				}
			}

			return metodos ;
		}
	])
})() ;