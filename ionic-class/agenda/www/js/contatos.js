( function(){
	angular.module( 'agenda.controller' )

	.controller( "ContatosController", 
		[ "ContatosModel", "Cache", "$state",
		function( ContatosModel, Cache, $state ) {
			this.contatos = ContatosModel.listarContatos() ;

			this.abrirFormulario = function( contato ) {
				Cache.set( 'Contato', contato ) ;
				$state.go( "app.contato-formulario" ) ;
			}

			this.novoContato = function() {
				Cache.set( 'Contato', {} ) ;
				$state.go( "app.contato-formulario" ) ;
			}
			
			this.removerContato = function( contato ) {
				console.log( contato ) ;
			}
		}
	])
})() ;