( function(){

  angular.module( 'agenda.controller', [] ) ;
  angular.module( 'agenda.model', [] ) ;
  angular.module( 'agenda.service', [] ) ;

  // Ionic Starter App

  // angular.module is a global place for creating, registering and retrieving Angular modules
  // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
  // the 2nd parameter is an array of 'requires'
  angular.module( 'agenda', [
    'ionic',
    'agenda.controller',
    'agenda.model',
    'agenda.service'
  ])

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .config(
      [ "$stateProvider", "$urlRouterProvider"
    , function( $stateProvider, $urlRouterProvider ) {

      $urlRouterProvider.otherwise( "/app/contatos" ) ;

      $stateProvider

      // container das abas / base de navegação
      .state( "app", {
        url: "/app",
        abstract: true,
        templateUrl: "view/tabs.html"
      })

      .state( "app.contatos", {
        url: "/contatos",
        views: {
          "contatos": {
            templateUrl: "view/contatos.html",
            controller: "ContatosController as ctCtrl"
          }
        }
      })

      .state( "app.contato-formulario", {
        url: "/contato-formulario",
        views: {
          "contatos": {
            templateUrl: "view/contato-formulario.html",
            controller: "ContatoFormularioController as cfCtrl"
          }
        }
      })



    }])

})() ;