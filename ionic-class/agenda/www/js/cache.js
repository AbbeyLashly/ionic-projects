( function(){
	angular.module( 'agenda.service' )

	.factory( "Cache", 
		[
		function(){
			var cache = [] ;
			
			return {
				set: function( key, value ){
					cache[key] = value ;
				},
				get: function( key ){
					if( cache[key] )
						return cache[key] ;

					return null ;
				},
				delete: function( key ){
					if( cache[key] )
						delete cache[key] ;
				}
			}
		}
	])
})() ;