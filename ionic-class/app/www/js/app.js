( function(){

	angular.module( "agenda.controller", [] ) ;
	angular.module( "agenda.model", [] ) ;
	angular.module( 'agenda.service', [] ) ;
	// Ionic Starter App

	// angular.module is a global place for creating, registering and retrieving Angular modules
	// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
	// the 2nd parameter is an array of 'requires'
	angular.module( 'agenda', [
		'ionic',
		'agenda.controller',
		'agenda.model',
		'agenda.service',
	])

	.run(function($ionicPlatform) {
		$ionicPlatform.ready(function() {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			if(window.cordova && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			}
			if(window.StatusBar) {
				StatusBar.styleDefault();
			}
		});
	})

	.config(
		[ '$stateProvider', '$urlRouterProvider',
	function( $stateProvider, $urlRouterProvider )
	{
		// Define a URL default
		$urlRouterProvider.otherwise( '/app/contatos' ) ;
		
		// Define os estados e rotas de url
		$stateProvider

		// Estado base do app
		.state( 'app', {
		    url: '/app',
		    abstract: true,
		    templateUrl: "view/tabs.html"
		})

		// Estados de transição do app
		.state( "app.contatos", {
			url: "/contatos",
			cache: false,
			views: {
				"app-contatos": {
					templateUrl: "view/contatos.html",
					controller: "ContatoController as ctCtrl"
				}
			}
		})
		
		.state( "app.contato-formulario", {
			url: "/contato-formulario",
			views: {
				"app-contatos": {
					templateUrl: "view/contato-formulario.html",
					controller: "ContatoFormularioController as cfCtrl"
				}
			}
		})
		
		.state( "app.favoritos", {
			url: "/favoritos",
			cache: false,
			views: {
				"app-favoritos": {
					templateUrl: "view/favoritos.html",
					controller: "FavoritoController as fvCtrl"
				}
			}
		})
		
		.state( "app.sobre", {
			url: "/sobre",
			views: {
				"app-info": {
					templateUrl: "view/sobre.html"
				}
			}
		})
	}])
})() ;