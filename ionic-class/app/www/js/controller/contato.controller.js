( function(){
	angular.module( 'agenda.controller' )

	.controller( 'ContatoController',
			[ 'ContatoModel', 'Cache', '$state',
		function( ContatoModel, Cache, $state )
		{
			this.contatos = ContatoModel.listarContatos() ;

			this.mostrarFormulario = function( contato ) {
				Cache.set( "contato", contato ) ;
				$state.go( "app.contato-formulario", {}, { location: "replace" } ) ;
			}

			this.removerContato = function( contato ) {
				ContatoModel.remover( contato.id ) ;
			}
		}
	])
})() ;