( function() {
	angular.module( 'agenda.controller' )

	.controller( "ContatoFormularioController", 
			[ "ContatoModel", "Cache", "$state",
		function( ContatoModel, Cache, $state )
		{
			this.contato = angular.copy(
				Cache.get( 'contato' )
			) ;
			
			this.imagens = [
				{ nome: "Yoda", imagem: "img/yoda.jpg" },
				{ nome: "Batman", imagem: "img/batman.jpg" },
				{ nome: "Harlequina", imagem: "img/harlequina.jpg" },
				{ nome: "Stormtrooper", imagem: "img/stormtrooper.jpg" },
				{ nome: "Tartarugas Ninjas", imagem: "img/turtle.jpg" }
			] ;

			this.guardar = function() {
				ContatoModel.guardar( this.contato ) ;
				$state.go( "app.contatos" ) ;
			}
		}
	]) ;
})() ;